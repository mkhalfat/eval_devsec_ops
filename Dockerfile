FROM ubuntu:20.10
RUN apt-get update && apt-get install curl=7.68.0-1ubuntu4.2 nginx=1.18.0-6ubuntu2 git=1:2.27.0-1ubuntu1 python3=3.8.6-0ubuntu1 -y --no-install-recommends && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
RUN addgroup esgi
RUN useradd -rm -d /home/jordan -s /bin/bash -g esgi -u 1001 jordan
RUN useradd -rm -d /home/jonathan -s /bin/bash -g esgi -u 1002 jonathan

COPY  index.html /var/www/html/
COPY  images/ /var/www/html

# Expose ports.
EXPOSE 80

# Define default command.
CMD ["nginx", "-g", "daemon off;"]