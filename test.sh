#!/bin/bash

docker exec test python3 --version
if [ $? == 0 ]
then
    echo "python3 est bien installé" 
else
    echo "python3 n'est pas installé" 
fi

docker exec test git --version
if [ $? == 0 ]
then
    echo "git est bien installé" 
else 
    echo "git n'est pas installé" 
fi

docker exec test service nginx start
if [ $? == 0 ]
then
    echo "nginx a bien démarré" 
else
    echo "nginx n'a pas démarré" 
fi

res=$(docker exec test curl 127.0.0.1)
$res == $(cat index.html)
if [ $? == 0 ]
then
    echo "le fichier index.html a été mis en place" 
else
    echo "le fichier index.html n'a pas été mis en place"  
fi

docker exec test grep jordan /etc/passwd
if [ $? == 0 ]
then
    echo "l'utilisateur jordan existe" 
else
    echo "l'utilisateur jordan n'existe pas" 
fi

docker exec test grep jonathan /etc/passwd
if [ $? == 0 ]
then
    echo "l'utilisateur jonathan existe" 
else
    echo "l'utilisateur jonathan n'existe pas" 
fi